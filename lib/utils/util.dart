import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';


class Util{

	static Future<String> getAddressUsingCoordinates(double latitude, double longitude) async {
		try {
			if (latitude == 0.0 || longitude == 0.0) {
				return '';
			}
			final coordinates = new Coordinates(latitude, longitude);
			List<Address> addresses =
			await Geocoder.local.findAddressesFromCoordinates(coordinates);

			print(addresses);
			if (addresses != null && addresses.length > 0) {
				Address first = addresses.first;
				print("${first.addressLine}");
				return "${first.addressLine}";
			} else {
				return '';
			}
		} on PlatformException catch (e) {
			print("decoding Address error $e");
			return '';
		}
	}
}