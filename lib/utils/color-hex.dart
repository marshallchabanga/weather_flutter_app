

import 'package:flutter/material.dart';

class ColorHex extends Color {
	static int _colorFromHex(String hex) {
		hex = hex.toUpperCase().replaceAll('#', '');
		if(hex.length == 6) {
			hex = 'FF'+hex;
		}
		return int.parse(hex, radix: 16);
	}
	ColorHex(final String value) : super(_colorFromHex(value));

}