
class Strings{
	static String GENERIC_ERROR_MESSAGE = "Something went wrong, try again";
	static String NETWORK_ERROR_MESSAGE = "Oops. Please check your network connectivity.";

	//other
	static String IMAGE_DIR = "images";
	static String APP_ID = "d528bf7dca606449c6df6b4ccd073ca9";
}