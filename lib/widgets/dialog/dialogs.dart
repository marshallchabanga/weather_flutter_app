import 'package:flutter/material.dart';
import 'package:flutter_app/model/favourite.dart';
import 'package:flutter_app/ui/home.dart';
import 'package:flutter_slidable/flutter_slidable.dart';


class Dialogs {
	static information(
			BuildContext context,
			String message, {
				String title,
				String positiveButtonLabel,
				String negativeButtonLabel,
				Function onPositiveButtonPressed,
				Function onNegativeButtonPressed,
				bool dismissible = true,
			}) {
		return showDialog(
				context: context,
				barrierDismissible: dismissible,
				builder: (BuildContext context) {
					return AlertDialog(
						title: title == null ? null : new Text('$title',
							style: new TextStyle(
								fontSize: 20.00,
								fontStyle: FontStyle.normal,
								color: Colors.blue
							),),
						content: new Text('$message',style: TextStyle(color: Colors.blue),),
						actions: <Widget>[
							new FlatButton(
								onPressed: onNegativeButtonPressed != null
										? onNegativeButtonPressed
										: () {
									Navigator.of(context).pop();
								},
								child: new Text(
									negativeButtonLabel != null
											? negativeButtonLabel.toUpperCase()
											: 'CANCEL',
								),
							),
							positiveButtonLabel == null
									? null
									: new FlatButton(
								onPressed: onPositiveButtonPressed != null
										? onPositiveButtonPressed
										: () {
									Navigator.of(context).pop();
								},
								child: new Text('${positiveButtonLabel.toUpperCase()}'),
							),
						],
					);
				});
	}

	static waiting(
			BuildContext context,
			String message, {
				String title,
				bool dismissible = false,
			}) {
		return showDialog(
				context: context,
				barrierDismissible: false,
				builder: (context) => Center(
					child: CircularProgressIndicator(
						backgroundColor: Colors.yellow,
					),
				));
	}

	static pickOption(
			BuildContext context,
			String message, {
				String title,
				List<SimpleDialogOption> options,
				bool dismissible = false,
			}) {
		return showDialog(
				context: context,
				barrierDismissible: dismissible,
				builder: (BuildContext context) {
					return SimpleDialog(
						title: title == null ? null : new Text(title,
								style: new TextStyle(
									fontSize: 18.0,
								)),
						children: options,
					);
				});
	}

	hideProgress(BuildContext context) {
		Navigator.pop(context);
	}



	static showFavouritesDialog(BuildContext context,List<Favourite> favourites,OnFavouriteSelected favouriteSelected) {
		return showDialog(
			context: context,
			builder: (BuildContext context) =>
			Material(
			  child: Column(
			    children: <Widget>[
			    	Padding(
			    	  padding: const EdgeInsets.only(top:20.0),
			    	  child: Text("Favourites",style: TextStyle(color: Colors.blue,fontSize: 18),),
			    	),
				    Padding(
			    	  padding: const EdgeInsets.only(top:20.0),
			    	  child: Text("swipe left to delete",style: TextStyle(color: Colors.grey,fontSize: 12),),
			    	),
			      Divider(),
			      new FavouritesList(favourites: favourites, favouriteSelected: favouriteSelected, context: context),
			    ],
			  ),
			),
		);
	}

}

class FavouritesList extends StatelessWidget {
  const FavouritesList({
    Key key,
    @required this.favourites,
    @required this.favouriteSelected,
    @required this.context,
  }) : super(key: key);

  final List<Favourite> favourites;
  final OnFavouriteSelected favouriteSelected;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
		return Padding(
			padding: const EdgeInsets.only(bottom : 8.0),
			child: ListView.builder(
				shrinkWrap: true,
				itemCount: favourites.length,
				itemBuilder: (context, position) {

					return(new FavouriteRow(favourite: favourites[position], favouriteSelected: favouriteSelected, context: context));
				},
			),
		);
	}
}

class FavouriteRow extends StatelessWidget {
  const FavouriteRow({
    Key key,
    @required this.favourite,
    @required this.favouriteSelected,
    @required this.context,
  }) : super(key: key);

  final Favourite favourite;
  final OnFavouriteSelected favouriteSelected;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {

		return Column(
		  children: <Widget>[
		    Slidable(
		    	actionPane: SlidableDrawerActionPane(),
		    	actionExtentRatio: 0.25,
		      child: Container(
		      	margin: EdgeInsets.only(left: 10,right: 10),
		      	height: 60,
		        child: Material(
		          child: InkWell(
		          	onTap: (){
		      		    favouriteSelected.favouriteSelected(favourite);
		      	    },
		            child: Row(
		            	children: <Widget>[
		            		Expanded(child: Text('${favourite.name}',style: TextStyle(color: Colors.blue),))
		            	],
		            ),
		          ),
		        ),
		      ),
		    		secondaryActions: <Widget>[
		    			IconSlideAction(
		    				caption: 'Delete',
		    				color: Colors.red,
		    				icon: Icons.delete,
						onTap: () {
		    					Navigator.of(context).pop();
							favouriteSelected.favouriteDeletion(favourite);
						},
		    			),
		    		]),
			  Divider()
		  ],
		);
	}
}
