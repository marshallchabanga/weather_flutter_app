import 'package:flutter_app/model/client-properties.dart';

import 'file-config-service.dart';


class ConfigService {

  ClientProperties readFileConfig() => FileConfigService.appProperties;

}
