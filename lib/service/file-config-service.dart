
import 'package:flutter_app/model/client-properties.dart';

class FileConfigService {
  static ClientProperties appProperties;

  void registerConfig(ClientProperties configProperties) {
    appProperties = configProperties;
  }
}
