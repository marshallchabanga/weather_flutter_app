import 'package:flutter_app/db/db-provider.dart';
import 'package:flutter_app/model/favourite.dart';

class FavouriteServiceImpl implements FavouriteService {
  Future<List<Favourite>> getAllFavourites() async {
    return DBProvider.db.getAllFavourites();
  }

  @override
  Future<Favourite> addFavourite(Favourite favourite) {
    return DBProvider.db.newFavourite(favourite);
  }

  @override
  Future<bool> deleteFavourite(String id) {
    return DBProvider.db.deleteFavourite(id);
  }

  @override
  Future<Favourite> updateFavourite(Favourite favourite) {
    return DBProvider.db.updateFavourite(favourite);
  }
}

abstract class FavouriteService {
  Future<List<Favourite>> getAllFavourites();

  Future<bool> deleteFavourite(String id);

  Future<Favourite> updateFavourite(Favourite favourite);

  Future<Favourite> addFavourite(Favourite favourite);
}
