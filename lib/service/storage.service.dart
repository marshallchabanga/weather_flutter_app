import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class StorageService {

  static String CURRENT_WEATHER="CURRENT_WEATHER";
  static String FORECAST_WEATHER="FORECAST_WEATHER";
  static String LAST_UPDATED="LAST_UPDATED";
  static Future<void> putInt(String key, int value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    preferences.setInt(key, value);
  }

  static Future<int> getInt(String key) async {
    int value;
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      value = preferences.getInt(key);
      return value;
    } catch (e) {
      print("$e");
      return value;
    }
  }

  static Future<void> putBool(String key, bool value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    preferences.setBool(key, value);
  }

  static Future<bool> getBool(String key) async {
    bool value;
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      value = preferences.getBool(key);
      return value;
    } catch (e) {
      print("$e");
      return value;
    }
  }

  static Future<bool> removeKey(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.remove(key);
  }

  static Future<bool> removeData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.clear();
  }

  static Future<void> putObjectList(String key, List<String> value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    preferences.setStringList(key, value);
  }

  static Future<List<String>> getObjectList(String key) async {
    List<String> value;
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      value = preferences.getStringList(key);
      return value;
    } catch (e) {
      print("$e");
      return value;
    }
  }

  static Future<void> putString(String key, String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    preferences.setString(key, value);
  }

  static Future<String> getString(String key) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.getString(key);
  }

  static Future<dynamic> putObject(String key, dynamic object) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String value = json.encode(object);

    return await preferences.setString(key, value);
  }

  static Future<dynamic> getObject(String key) async {
    dynamic object;
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String value = preferences.getString(key);
      dynamic object = json.decode(value);
      return object;
    } catch (e) {
      print("$e");
      return object;
    }
  }
}
