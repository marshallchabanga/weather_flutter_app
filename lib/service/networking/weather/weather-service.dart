



import 'package:flutter_app/dto/request/location-coordinates-dto.dart';
import 'package:flutter_app/dto/response/current-weather-dto.dart';
import 'package:flutter_app/dto/response/day-forecast-dto.dart';
import 'package:flutter_app/utils/strings.dart';

import '../http-service.dart';
import '../network_service_response.dart';

class WeatherService{


	final HttpService _httpService;
	WeatherService(this._httpService);

	Future<NetworkServiceResponse<CurrentWeather>> currentWeather(LocationCoordinatesDto requestDto) async {

		String endpoint= "/weather";

		Map<String,String> params= Map();
		params["lat"] ='${requestDto.latitude}';
		params["lon"] ='${requestDto.longitude}';
		params["APPID"] =Strings.APP_ID;
		MappedNetworkServiceResponse<CurrentWeather> result =await _httpService.getAsync(endpoint,params: params);

		if (result.mappedResult != null) {
			var res = CurrentWeather.fromJson(result.mappedResult);
			return new NetworkServiceResponse(
				content: res,
				success: result.networkServiceResponse.success,
			);
		} else {
			return new NetworkServiceResponse(success: result.networkServiceResponse.success, message: result.networkServiceResponse.message);
		}

	}
	Future<NetworkServiceResponse<DaysForecast>> forecastWeather(LocationCoordinatesDto requestDto) async {

		String endpoint= "/forecast";

		Map<String,String> params= Map();
		params["lat"] ='${requestDto.latitude}';
		params["lon"] ='${requestDto.longitude}';
		params["APPID"] =Strings.APP_ID;
		MappedNetworkServiceResponse<DaysForecast> result =await _httpService.getAsync(endpoint,params: params);

		if (result.mappedResult != null) {
			var res = DaysForecast.fromJson(result.mappedResult);
			return new NetworkServiceResponse(
				content: res,
				success: result.networkServiceResponse.success,
			);
		} else {
			return new NetworkServiceResponse(success: result.networkServiceResponse.success, message: result.networkServiceResponse.message);
		}

	}



}