import 'dart:io';


final httpSecurityContext = new SecurityContext(withTrustedRoots: false);

class PublicKeyPinningHttpOverrides extends HttpOverrides {

  @override
  HttpClient createHttpClient(SecurityContext context) {
    context = httpSecurityContext;
    return super.createHttpClient(context)
      ..badCertificateCallback = ((X509Certificate x509Certificate, String host, int port) {
          return false;

      });
  }
}
