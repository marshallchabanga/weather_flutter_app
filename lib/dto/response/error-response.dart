import 'package:json_annotation/json_annotation.dart';
import 'package:flutter_app/dto/response/error-dto.dart';
part 'error-response.g.dart';

@JsonSerializable()
class ErrorResponse {


	Error error;

	ErrorResponse({this.error});

	factory ErrorResponse.fromJson(Map<String, dynamic> json) =>
			_$ErrorResponseFromJson(json);

	Map<String, dynamic> toJson() => _$ErrorResponseToJson(this);

}



