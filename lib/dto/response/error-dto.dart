
import 'package:json_annotation/json_annotation.dart';

part 'error-dto.g.dart';

@JsonSerializable()
class Error{
	int status;
	String code;
	String message;
	String field;

	Error({this.status, this.code, this.message, this.field});

	factory Error.fromJson(Map<String, dynamic> json) =>
			_$ErrorFromJson(json);

	Map<String, dynamic> toJson() => _$ErrorToJson(this);

}