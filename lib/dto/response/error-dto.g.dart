// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error-dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Error _$ErrorFromJson(Map<String, dynamic> json) {
  return Error(
      status: json['status'] as int,
      code: json['code'] as String,
      message: json['message'] as String,
      field: json['field'] as String);
}

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'status': instance.status,
      'code': instance.code,
      'message': instance.message,
      'field': instance.field
    };
