import 'dart:async';

import 'package:flutter_app/dto/request/location-coordinates-dto.dart';
import 'package:flutter_app/dto/response/current-weather-dto.dart';
import 'package:flutter_app/dto/response/day-forecast-dto.dart';
import 'package:flutter_app/model/fetch_process.dart';
import 'package:flutter_app/service/networking/network_service_response.dart';
import 'package:flutter_app/service/networking/weather/weather-service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/service/storage.service.dart';
import 'package:flutter_app/widgets/dialog/dialogs.dart';
import 'package:kiwi/kiwi.dart' as kiwi;
import 'package:flutter_app/action/weather-actions.dart';
import 'package:rxdart/rxdart.dart';

class WeatherBloc {
  BuildContext _callerContext;
  FetchProcess apiCall = new FetchProcess();
  FetchProcess apiFetchCall = new FetchProcess();
  static bool isUserNotified = false;

  //Services
  final WeatherService _weatherService = kiwi.Container().resolve<WeatherService>();

  //Controllers
  final _weatherActionController = StreamController<WeatherAction>();

  Sink<WeatherAction> get eventSink => _weatherActionController.sink;

  final _apiController = BehaviorSubject<FetchProcess>();
  final _apiForecastController = BehaviorSubject<FetchProcess>();

  Stream<FetchProcess> get apiResult => _apiController.stream;

  Stream<FetchProcess> get apiForecastResult => _apiForecastController.stream;

  WeatherBloc(BuildContext context) {
    _callerContext = context;
    _weatherActionController.stream.listen(_onWeatherAction);
  }

  void _onWeatherAction(WeatherAction event) {
    if (event is CurrentWeatherAction) {
      getCurrentWeather(event.coordinatesDto);
    } else if (event is ForecastWeatherAction) {
      forecastWeather(event.coordinatesDto);
    } else if (event is LocalFetchWeatherAction) {
      loadWeatherFromLocal();
    }
  }

  void getCurrentWeather(LocationCoordinatesDto coordinatesDto) async {
    NetworkServiceResponse<CurrentWeather> response = await _weatherService.currentWeather(coordinatesDto);
    apiCall.response = response;

    if (response.success) {
      StorageService.putObject(StorageService.LAST_UPDATED,DateTime.now().millisecondsSinceEpoch);
      _apiController.add(apiCall);
    }
  }

  void forecastWeather(LocationCoordinatesDto coordinatesDto) async {
    NetworkServiceResponse<DaysForecast> response = await _weatherService.forecastWeather(coordinatesDto);
    apiFetchCall.response = response;
    if (response.success) {
      _apiForecastController.add(apiFetchCall);
    } else {
      if (isUserNotified == false) {
        Dialogs.information(_callerContext, apiCall.response.message, title: "Error");
        isUserNotified = true;
      }
    }
  }

  loadWeatherFromLocal() async {
    Map<String, dynamic> currentWeatherMap = await StorageService.getObject(StorageService.CURRENT_WEATHER);
    Map<String, dynamic> forecastWeather = await StorageService.getObject(StorageService.FORECAST_WEATHER);

    if (currentWeatherMap != null && forecastWeather != null) {
      DaysForecast daysForecast = DaysForecast.fromJson(forecastWeather);
      CurrentWeather currentWeather = CurrentWeather.fromJson(currentWeatherMap);

      print("current_weather ${currentWeather.toJson()}");
      NetworkServiceResponse<CurrentWeather> response = NetworkServiceResponse(
        content: currentWeather,
        success: true,
      );
      apiCall.response = response;

      if (response.success) {
        _apiController.add(apiCall);
      }

      NetworkServiceResponse<DaysForecast> responseForecast = NetworkServiceResponse(
        content: daysForecast,
        success: true,
      );
      apiFetchCall.response = responseForecast;

      if (response.success) {
        _apiForecastController.add(apiFetchCall);
      }
    }
  }

  dispose() {
    _apiController?.close();
    _apiForecastController?.close();
    _weatherActionController?.close();
  }
}
