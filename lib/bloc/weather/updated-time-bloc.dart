import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_app/action/weather-actions.dart';
import 'package:flutter_app/service/storage.service.dart';
import 'package:intl/intl.dart';

class UpdatedTimeBloc {
  final timeEventController = StreamController<String>();

  StreamSink<String> get _timeEventSink => timeEventController.sink;

  Stream<String> get timeEventStream => timeEventController.stream;

  final timeOEventController = StreamController<WeatherAction>();

  Sink<WeatherAction> get timeOEvent => timeOEventController.sink;

  UpdatedTimeBloc(BuildContext context) {
    timeOEventController.stream.listen(_onTimeEventEvent);
  }

  void _onTimeEventEvent(WeatherAction event) async {
    if (event is UpdatedTimeAction) {
      int lastUpdated = await StorageService.getObject(StorageService.LAST_UPDATED);

      DateTime dateTime=DateTime.fromMillisecondsSinceEpoch(lastUpdated);
      String dt =DateFormat.yMEd().add_jms().format(dateTime);

      _timeEventSink.add(dt);
    }
  }

  dispose() {
    timeEventController?.close();
    timeOEventController?.close();
  }
}
