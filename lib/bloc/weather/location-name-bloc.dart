import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_app/action/weather-actions.dart';
import 'package:flutter_app/utils/util.dart';
class LocationNameBloc {
  final locNameEventController = StreamController<String>();

  StreamSink<String> get _locNameEventSink => locNameEventController.sink;

  Stream<String> get locNameEventStream => locNameEventController.stream;

  final locNameOEventController = StreamController<WeatherAction>();

  Sink<WeatherAction> get locNameOEvent => locNameOEventController.sink;

  LocationNameBloc(BuildContext context) {
    locNameOEventController.stream.listen(_onTimeEventEvent);
  }

  void _onTimeEventEvent(WeatherAction event) async {
    if (event is LocationNameTimeAction) {
     double lat = event.latitude;
     double lon = event.longitude;

      String locName = await Util.getAddressUsingCoordinates(lat,lon);
     _locNameEventSink.add("$locName");
    }
  }

  dispose() {
    locNameEventController?.close();
    locNameOEventController?.close();
  }
}
