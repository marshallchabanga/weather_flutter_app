import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/action/favourite-actions.dart';
import 'package:flutter_app/model/favourite.dart';
import 'package:flutter_app/service/favourite-service.dart';
import 'package:rxdart/rxdart.dart';

class FavouriteBloc {
  FavouriteServiceImpl favouriteServiceImpl;
  BuildContext _callerContext;

  //Controllers
  final _favouriteActionController = StreamController<FavouriteAction>();
  Sink<FavouriteAction> get favouriteSink => _favouriteActionController.sink;



  final _deleteFavoriteController = BehaviorSubject<bool>();
  final _updateFavoriteController = BehaviorSubject<Favourite>();
  final _newFavoriteController = BehaviorSubject<Favourite>();
  final _allFavoriteController = BehaviorSubject<List<Favourite>>();

  Stream<bool> get deleteFavoriteStream => _deleteFavoriteController.stream;

  Stream<Favourite> get updateFavoriteStream => _updateFavoriteController.stream;

  Stream<Favourite> get newFavoriteStream => _newFavoriteController.stream;

  Stream<List<Favourite>> get allFavoriteStream => _allFavoriteController.stream;

  FavouriteBloc(BuildContext context) {
    _callerContext = context;
    favouriteServiceImpl = new FavouriteServiceImpl();
    _favouriteActionController.stream.listen(_onFavouriteAction);
  }


  void _onFavouriteAction(FavouriteAction event) {
    if (event is DeleteFavouriteAction) {
      _deleteFav(event.id);
    }else if (event is NewFavouriteAction) {

      _newFav(event.favourite);
    }
    else if (event is UpdateFavouriteAction) {
      _updateFav(event.favourite);
    }
    else if (event is AllFavouriteAction) {
      _allFavourite();
    }
  }

  _deleteFav(String id)async{

   bool isDeleted=await favouriteServiceImpl.deleteFavourite(id);
   _deleteFavoriteController.add(isDeleted);
  }

  _updateFav(Favourite favourite)async{
    Favourite updatedFav=await favouriteServiceImpl.updateFavourite(favourite);
    debugPrint('updated favourite=> ${updatedFav.toJson()}');
    _updateFavoriteController.add(updatedFav);
  }

  _newFav(Favourite favourite)async{

    Favourite newFav=await favouriteServiceImpl.addFavourite(favourite);
    debugPrint('new favourite=> ${newFav.toJson()}');

    _newFavoriteController.add(newFav);
  }

  _allFavourite() async{

    List<Favourite> favorites=await favouriteServiceImpl.getAllFavourites();

    favorites.forEach((data){
      debugPrint('${data.toJson()}');
    });

    _allFavoriteController.add(favorites);


  }

  dispose() {
    _favouriteActionController?.close();
    _deleteFavoriteController?.close();
    _updateFavoriteController?.close();
    _newFavoriteController?.close();
    _allFavoriteController?.close();
  }
}
