import 'dart:async';

import 'package:flutter_app/action/favourite-actions.dart';
import 'package:flutter_app/bloc/favourite/favourite-bloc.dart';
import 'package:flutter_app/bloc/weather/location-name-bloc.dart';
import 'package:flutter_app/bloc/weather/updated-time-bloc.dart';
import 'package:flutter_app/bloc/weather/weather-bloc.dart';
import 'package:flutter_app/db/db-provider.dart';
import 'package:flutter_app/model/favourite.dart';
import 'package:flutter_app/model/fetch_process.dart';
import 'package:flutter_app/action/weather-actions.dart';
import 'package:flutter_app/service/storage.service.dart';
import 'package:flutter_app/utils/image-icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kiwi/kiwi.dart' as kiwi;
import 'package:location/location.dart';

import '../dto/request/location-coordinates-dto.dart';
import '../dto/response/current-weather-dto.dart';
import '../dto/response/day-forecast-dto.dart';
import '../service/networking/weather/weather-service.dart';
import '../widgets/dialog/dialogs.dart';
import 'package:google_places_picker/google_places_picker.dart';
import 'package:flutter_app/utils/color-hex.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> implements OnFavouriteSelected {
  WeatherService weatherService = kiwi.Container().resolve<WeatherService>();
  WeatherBloc weatherBloc;
  UpdatedTimeBloc timeBloc;
  FavouriteBloc favouriteBloc;
  LocationNameBloc locationNameBloc;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isRefresh = false;
  Color drawerColor = Colors.blue;
  String locationName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(child: Form(child: buildForm())),
      drawer: buildDrawer(),
    );
  }

  Drawer buildDrawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              children: <Widget>[
                Text(
                  'Weather Forecast',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Created by Marshall Takudzwa Chabanga',
                  style: TextStyle(color: Colors.white, fontSize: 10),
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: drawerColor,
            ),
          ),
          ListTile(
              title: Text('View Favourites', style: TextStyle(color: Colors.blue, fontSize: 14)),
              onTap: () {
                Navigator.pop(context);
                favouriteBloc.favouriteSink.add(AllFavouriteAction());
              },
              trailing: Icon(
                Icons.favorite,
                color: Colors.blue,
                size: 30.0,
              )),
          Divider(),
          ListTile(
              title: Text('Add Favourite', style: TextStyle(color: Colors.blue, fontSize: 14)),
              onTap: () {
                Navigator.pop(context);
                _addFavourite();
              },
              trailing: Icon(
                Icons.add,
                color: Colors.blue,
                size: 30.0,
              )),
          Divider(),
          ListTile(
            title: Text('Refresh', style: TextStyle(color: Colors.blue, fontSize: 14)),
            onTap: () {
              isRefresh = true;
              Navigator.pop(context);
              currentLocationService(context);
            },
            trailing: Icon(
              Icons.refresh,
              color: Colors.blue,
              size: 30.0,
            ),
          ),
          Divider(),
          ListTile(
            title: Text('Search', style: TextStyle(color: Colors.blue, fontSize: 14)),
            onTap: () {
              isRefresh = true;
              Navigator.pop(context);
              _onceSearch();
            },
            trailing: Icon(
              Icons.search,
              color: Colors.blue,
              size: 30.0,
            ),
          ),
          Divider(),
        ],
      ),
    );
  }

  Widget buildForm() {
    return StreamBuilder<FetchProcess>(
        stream: weatherBloc.apiResult,
        builder: (context, snapshot) {
          return Container(
            color: snapshot.hasData ? backgroundColor(snapshot.data.response.content) : Colors.grey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                currentWeatherWidget(),
                forecastWeatherWidget(),
              ],
            ),
          );
        });
  }

  Widget currentWeatherWidget() {
    return Flexible(
      flex: 0,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.5,
        child: Stack(
          children: <Widget>[
            StreamBuilder<FetchProcess>(
                stream: weatherBloc.apiResult,
                builder: (context, snapshot) {
                  return snapshot.hasData ? buildTopImage(snapshot.data.response.content) : Container();
                }),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.05,
              left: MediaQuery.of(context).size.height * 0.02,
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _scaffoldKey.currentState.openDrawer();
                    },
                    child: Container(
                      width: 30,
                      height: 30,
                      child: SvgPicture.asset(
                        "res/images/icon_menu.svg",
                        width: 20.0,
                        height: 20.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.05,
              right: MediaQuery.of(context).size.height * 0.02,
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      isRefresh = true;
                      currentLocationService(context);
                    },
                    child: Container(
                      width: 30,
                      height: 30,
                      child: Icon(
                        Icons.refresh,
                        color: Colors.white,
                        size: 30.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Center(
                child: StreamBuilder<FetchProcess>(
                    stream: weatherBloc.apiResult,
                    builder: (context, snapshot) {
                      return snapshot.hasData
                          ? buildCurrentWeatherData(snapshot.data.response.content)
                          : Container(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Check your internet connetion and also enable location service and "
                                  "tap refresh icon",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white, fontSize: 18),
                                ),
                              ),
                            );
                    })),
          ],
        ),
      ),
    );
  }

  Color backgroundColor(CurrentWeather weather) {
    if (weather == null) {
      return ColorHex("#54717A");
    }
    String title = weather.weather[0].main;
    String currentWeather = title.toLowerCase();
    if (currentWeather.contains("cloud".toLowerCase())) {
      return ColorHex("#54717A");
    } else if (currentWeather.contains("sun".toLowerCase())) {
      return ColorHex("#47AB2F");
    } else if (currentWeather.contains("clear".toLowerCase())) {
      return ColorHex("#47AB2F");
    } else if (currentWeather.contains("rain".toLowerCase())) {
      return ColorHex("#57575D");
    }
    return ColorHex("#54717A");
  }

  Image buildTopImage(CurrentWeather weather) {
    if (weather == null) {
      return Image.asset("res/images/forest_cloudy.png");
    }
    String title = weather.weather[0].main;
    String currentWeather = title.toLowerCase();
    if (currentWeather.contains("cloud".toLowerCase())) {
      return Image.asset("res/images/forest_cloudy.png");
    } else if (currentWeather.contains("sun".toLowerCase())) {
      return Image.asset("res/images/forest_sunny.png");
    } else if (currentWeather.contains("clear".toLowerCase())) {
      return Image.asset("res/images/forest_sunny.png");
    } else if (currentWeather.contains("rain".toLowerCase())) {
      return Image.asset("res/images/forest_rainy.png");
    }
    return Image.asset("res/images/forest_cloudy.png");
  }

  Widget buildCurrentWeatherData(CurrentWeather weather) {
    double tmp = weather.main.temp;
    double temp = tmp - 273.15;

    String title = weather.weather[0].main;
    timeBloc.timeOEvent.add(UpdatedTimeAction());
    if (coordinatesDto != null) {
      locationNameBloc.locNameOEvent.add(LocationNameTimeAction(coordinatesDto.latitude, coordinatesDto.longitude));
    }

    return StreamBuilder<FetchProcess>(
        stream: weatherBloc.apiResult,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              snapshot.hasData
                  ? Text(
                      "${temp.round()}°\n ${title.toUpperCase()}",
                      style: TextStyle(fontSize: 42, fontWeight: FontWeight.bold, color: Colors.white),
                      textAlign: TextAlign.center,
                    )
                  : Container(),
              SizedBox(
                height: 20,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  StreamBuilder<String>(
                      stream: timeBloc.timeEventStream,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Text(
                                "Last Updated : ${snapshot.data}",
                                style: TextStyle(fontSize: 12, color: Colors.white),
                                textAlign: TextAlign.center,
                              )
                            : Container();
                      }),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0, top: 8),
                        child: Icon(Icons.location_on, color: Colors.white, size: 20.0),
                      ),
                      StreamBuilder<String>(
                          stream: locationNameBloc.locNameEventStream,
                          builder: (context, snapshot) {
                            return Expanded(
                              child: snapshot.hasData
                                  ? Text(
                                      "${snapshot.data}",
                                      style: TextStyle(fontSize: 12, color: Colors.white),
                                      textAlign: TextAlign.center,
                                    )
                                  : Container(),
                            );
                          }),
                    ],
                  ),
                ],
              )
            ],
          );
        });
  }

  Widget forecastWeatherWidget() {
    return Flexible(
      flex: 0,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.5,
        child: StreamBuilder<FetchProcess>(
            stream: weatherBloc.apiForecastResult,
            builder: (context, snapshot) {
              return SingleChildScrollView(
                physics: ScrollPhysics(),
                child: snapshot.hasData ? buildForecastContainer(context, snapshot.data.response.content) : Container(),
              );
            }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    DBProvider.db.initDB();
    weatherBloc = WeatherBloc(context);
    favouriteBloc = FavouriteBloc(context);
    timeBloc = UpdatedTimeBloc(context);
    locationNameBloc = LocationNameBloc(context);

    PluginGooglePlacePicker.initialize(
      androidApiKey: "AIzaSyCsF5IIXuppjvVFwTReB7LG7b1MiwAIYo0",
      iosApiKey: "AIzaSyCsF5IIXuppjvVFwTReB7LG7b1MiwAIYo0",
    );
    loadWeatherFromLocal();

    favouriteBloc.newFavoriteStream.listen((favourite) {
      if (favourite != null && favourite.id != 0) {
        Dialogs.information(context, "${favourite.name} was added to your favourites", title: "Success", negativeButtonLabel: "Close");
      }
    });

    favouriteBloc.allFavoriteStream.listen((favourites) {
      if (favourites == null || favourites.isEmpty) {
        Dialogs.information(context, "You do not have any favourites", title: "Empty Favourites", negativeButtonLabel: "Close");
        return;
      }
      Dialogs.showFavouritesDialog(context, favourites, this);
    });
    favouriteBloc.deleteFavoriteStream.listen((isDeleted) {
      Navigator.of(context).pop();
      if (isDeleted == null || isDeleted == true) {
        Dialogs.information(context, "Favourite was deleted successful", title: "Favourite Deleted", negativeButtonLabel: "Close");
      } else {
        Dialogs.information(context, "Favourite was not deleted, please try again", title: "Error");
      }
    });
  }

  loadWeatherFromLocal() async {
    Map<String, dynamic> currentWeatherMap = await StorageService.getObject(StorageService.CURRENT_WEATHER);
    Map<String, dynamic> forecastWeather = await StorageService.getObject(StorageService.FORECAST_WEATHER);

    if (currentWeatherMap != null && forecastWeather != null) {
      CurrentWeather currentWeather = CurrentWeather.fromJson(currentWeatherMap);

      coordinatesDto = new LocationCoordinatesDto(currentWeather.coord.lat, currentWeather.coord.lon);

      weatherBloc.eventSink.add(LocalFetchWeatherAction());
    } else {
      currentLocationService(context);
    }
  }

  _addFavourite() async {
    var place = await PluginGooglePlacePicker.showAutocomplete(mode: PlaceAutocompleteMode.MODE_OVERLAY, typeFilter: TypeFilter.ESTABLISHMENT);

    Favourite favourite = Favourite(name: place.name, latitude: place.latitude, longitude: place.longitude);

    if (!mounted) {
      return;
    } else {
      NewFavouriteAction favouriteAction = NewFavouriteAction(favourite);

      favouriteBloc.favouriteSink.add(favouriteAction);
    }
  }

  _onceSearch() async {
    var place = await PluginGooglePlacePicker.showAutocomplete(mode: PlaceAutocompleteMode.MODE_OVERLAY, typeFilter: TypeFilter.ESTABLISHMENT);
    coordinatesDto = new LocationCoordinatesDto(place.latitude, place.longitude);

    if (!mounted) {
      return;
    } else {
      coordinatesDto = new LocationCoordinatesDto(place.latitude, place.longitude);
      weatherBloc.eventSink.add(CurrentWeatherAction(coordinatesDto));
      weatherBloc.eventSink.add(ForecastWeatherAction(coordinatesDto));
    }
  }

  @override
  void dispose() {
    weatherBloc?.dispose();
    favouriteBloc?.dispose();
    super.dispose();
  }

  bool hasPermission;
  LocationData locationData;
  CurrentWeather currentWeather;
  DaysForecast daysForecast;
  LocationCoordinatesDto coordinatesDto;
  Location location;

  Future<LocationData> currentLocationService(BuildContext context) async {
    try {
      location = new Location();
      location.requestPermission().then((granted) {
        location.onLocationChanged().listen((LocationData currentLocation) {
          this.locationData = currentLocation;
          coordinatesDto = new LocationCoordinatesDto(this.locationData.latitude, this.locationData.longitude);

          if (this.currentWeather == null || isRefresh == true) {
            weatherBloc.eventSink.add(CurrentWeatherAction(coordinatesDto));
          }

          if (this.daysForecast == null || isRefresh == true) {
            weatherBloc.eventSink.add(ForecastWeatherAction(coordinatesDto));
          }
          isRefresh = false;

          return currentLocation;
        });
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        Dialogs.information(context, "Grant location permission", title: "Permission Denied");
      }
    }
    return null;
  }

  Widget buildForecastContainer(BuildContext context, DaysForecast forecast) {
    this.daysForecast = forecast;
    return Container(
      child: Column(
        children: <Widget>[
          buildMinCurrentMaxRow(),
          Divider(color: Colors.white,),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: forecast.list.length,
            itemBuilder: (context, position) {
              ListData listData = forecast.list[position];
              return foreCastRow(listData);
            },
          ),
        ],
      ),
    );
  }

  Widget foreCastRow(ListData listData) {
    double tmp = listData.main.temp;
    double temp = tmp - 273.15;
    String tempString = "${temp.round()}°";

    IconData iconData = getIconData(listData.weather[0].icon);

    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10),
            width: MediaQuery.of(context).size.width*0.2,
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text('${DateFormat('E, ha').format(DateTime.fromMillisecondsSinceEpoch(listData.dt * 1000))}',
                    style: TextStyle(color: Colors.white))),
          ),
          Container(
            child: Align(
                alignment: Alignment.center,
                child: iconData == null
                    ? Container
                    : Icon(
                        iconData,
                        color: Colors.white,
                        size: 20,
                      )),
          ),
          Container(width: 50, child: Align(alignment: Alignment.centerLeft, child: Text('$tempString', style: TextStyle(color: Colors.white)))),
        ],
      ),
    );
  }

  buildMinCurrentMaxRow() {
    return Container(
      padding: EdgeInsets.only(top: 10,right: 25),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
          width: MediaQuery.of(context).size.width*0.2,
          child: Align(
            alignment: Alignment.center,
            child: StreamBuilder<FetchProcess>(
                stream: weatherBloc.apiResult,
                builder: (context, snapshot) {
                  return snapshot.hasData ? buildMin(snapshot.data.response.content) : Container();
                }),
          ),
        ),
        Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: StreamBuilder<FetchProcess>(
                stream: weatherBloc.apiResult,
                builder: (context, snapshot) {
                  return snapshot.hasData ? new CurrentWidget(currentWeather: snapshot.data.response.content) : Container();
                }),
          ),
        ),
        Container(
          child: Align(
            alignment: Alignment.centerRight,
            child: StreamBuilder<FetchProcess>(
                stream: weatherBloc.apiResult,
                builder: (context, snapshot) {
                  return snapshot.hasData ? new MaxWidget(currentWeather: snapshot.data.response.content) : Container();
                }),
          ),
        ),
      ]),
    );
  }

  Widget buildMin(CurrentWeather currentWeather) {
    double tmp = currentWeather.main.tempMin;

    this.currentWeather = currentWeather;
    double tempMin = tmp - 273.15;
    int min = tempMin.round();
    return Column(
      children: <Widget>[
        Text(
          '$min°'.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 17),
        ),
        Text(
          'min',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }

  @override
  void favouriteSelected(Favourite favourite) {
    this.locationName = favourite.name;
    Navigator.of(context).pop();
    coordinatesDto = new LocationCoordinatesDto(favourite.latitude, favourite.longitude);
    weatherBloc.eventSink.add(CurrentWeatherAction(coordinatesDto));
    weatherBloc.eventSink.add(ForecastWeatherAction(coordinatesDto));
  }

  @override
  void favouriteDeletion(Favourite favourite) {
    Dialogs.information(context, "Are you sure you want to delete location ${favourite.name}?", positiveButtonLabel: "Yes", negativeButtonLabel: "No",
        onPositiveButtonPressed: () {
      DeleteFavouriteAction deleteFavouriteAction = new DeleteFavouriteAction('${favourite.id}');
      favouriteBloc.favouriteSink.add(deleteFavouriteAction);
    });
  }
}

class CurrentWidget extends StatelessWidget {
  const CurrentWidget({
    Key key,
    @required this.currentWeather,
  }) : super(key: key);

  final CurrentWeather currentWeather;

  @override
  Widget build(BuildContext context) {
    StorageService.putObject(StorageService.CURRENT_WEATHER, currentWeather);

    double tmp = currentWeather.main.temp;
    double temp = tmp - 273.15;
    int min = temp.round();

    return Column(
      children: <Widget>[
        Text(
          '$min°'.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 17),
        ),
        Text(
          'Current',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }
}

class MaxWidget extends StatelessWidget {
  const MaxWidget({
    Key key,
    @required this.currentWeather,
  }) : super(key: key);

  final CurrentWeather currentWeather;

  @override
  Widget build(BuildContext context) {
    double tmp = currentWeather.main.tempMax;
    double temp = tmp - 273.15;
    int min = temp.round();
    return Column(
      children: <Widget>[
        Text(
          '$min°'.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 17),
        ),
        Text(
          'max',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }
}

abstract class OnFavouriteSelected {
  void favouriteSelected(Favourite favourite);

  void favouriteDeletion(Favourite favourite);
}
