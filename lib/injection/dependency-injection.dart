import 'package:flutter_app/service/config.service.dart';
import 'package:flutter_app/service/networking/http-service.dart';
import 'package:flutter_app/service/networking/weather/weather-service.dart';
import 'package:flutter_app/service/storage.service.dart';
import 'package:http/http.dart' as http;
import 'package:kiwi/kiwi.dart' as kiwi;


void initKiwi() {
	kiwi.Container()
		..registerInstance(http.Client())
		..registerFactory((c) => ConfigService())
		..registerFactory((c) => HttpService(c.resolve()))
		..registerFactory((c) => StorageService())
		..registerFactory((c) => WeatherService(c.resolve()))
	;
}
