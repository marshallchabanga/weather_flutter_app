

import 'package:flutter_app/dto/request/location-coordinates-dto.dart';

abstract class WeatherAction {}
class CurrentWeatherAction extends WeatherAction{
	LocationCoordinatesDto coordinatesDto;
	CurrentWeatherAction(this.coordinatesDto);


}

class ForecastWeatherAction extends WeatherAction{
	LocationCoordinatesDto coordinatesDto;
	ForecastWeatherAction(this.coordinatesDto);


}

class LocalFetchWeatherAction extends WeatherAction{
	LocalFetchWeatherAction();


}

class UpdatedTimeAction extends WeatherAction{
	UpdatedTimeAction();


}

class LocationNameTimeAction extends WeatherAction{

	double latitude;
	double longitude;

	LocationNameTimeAction(this.latitude, this.longitude);


}