import 'package:flutter_app/model/favourite.dart';

abstract class FavouriteAction {}


class DeleteFavouriteAction extends FavouriteAction{

	String id;

	DeleteFavouriteAction(this.id);


}

class NewFavouriteAction extends FavouriteAction{

	Favourite favourite;

	NewFavouriteAction(this.favourite);


}

class UpdateFavouriteAction extends FavouriteAction{
	Favourite favourite;

	UpdateFavouriteAction(this.favourite);

}

class AllFavouriteAction extends FavouriteAction{

}