import 'package:json_annotation/json_annotation.dart';

part 'client-properties.g.dart';

@JsonSerializable()
class ClientProperties {
	String appId;
	String applicationName;
	String applicationDisplayName;
	String host;
	String baseUrl;


	ClientProperties({
		this.appId,
		this.applicationName,
		this.applicationDisplayName,
		this.host,
		this.baseUrl,
		});

	factory ClientProperties.fromJson(Map<String, dynamic> json) =>
			_$ClientPropertiesFromJson(json);

	Map<String, dynamic> toJson() => _$ClientPropertiesToJson(this);
}
