

class Favourite{
	int id;
	String name;
	double latitude;
	double longitude;

	Favourite({this.id, this.name, this.latitude, this.longitude});

	Map<String, dynamic> toJson() => {
		"id": id,
		"name": name,
		"latitude": latitude,
		"longitude": longitude,
	};

	factory Favourite.fromJson(Map<String, dynamic> json) {
		return new Favourite(
				id: json["id"],
				name: json["name"],
				latitude: json["latitude"],
				longitude: json["longitude"],);
	}



}