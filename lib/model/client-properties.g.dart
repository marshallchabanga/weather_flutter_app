part of 'client-properties.dart';

ClientProperties _$ClientPropertiesFromJson(Map<String, dynamic> json) {
  return ClientProperties(
      appId: json['appId'] as String,
      applicationName: json['applicationName'] as String,
      applicationDisplayName: json['applicationDisplayName'] as String,
      host: json['host'] as String,
      baseUrl: json['baseUrl'] as String,);
}

Map<String, dynamic> _$ClientPropertiesToJson(ClientProperties instance) =>
    <String, dynamic>{
      'appId': instance.appId,
      'applicationName': instance.applicationName,
      'applicationDisplayName': instance.applicationDisplayName,
      'host': instance.host,
      'baseUrl': instance.baseUrl
    };
