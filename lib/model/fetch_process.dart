

import 'package:flutter_app/service/networking/network_service_response.dart';

enum ApiType {
  performCurrentWeather,
  performForecastWeather
}

class FetchProcess<T> {
  ApiType type;
  bool loading;
  NetworkServiceResponse<T> response;

  FetchProcess({this.loading, this.response, this.type});
}
