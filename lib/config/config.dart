
import 'package:flutter_app/model/client-properties.dart';

class Config {

  static final String host = 'api.openweathermap.org';
  static final String baseUrl = 'data/2.5';
  static final String applicationId = 'weather-app-01';
  static final String applicationName = 'Weather-App';
  static final String applicationDisplayName = 'Weather App';

  static ClientProperties appProperties() {
    return ClientProperties(
      appId: applicationId,
      baseUrl: baseUrl,
      host: host,
      applicationName: applicationName,
      applicationDisplayName: applicationDisplayName
    );
  }
}
