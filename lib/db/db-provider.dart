import 'dart:async';
import 'dart:io';

import 'package:flutter_app/model/favourite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
	DBProvider._();

	static final DBProvider db = DBProvider._();

	static Database _database;

	Future<Database> get database async {
		if (_database != null) return _database;

		// if _database is null we instantiate it
		_database = await initDB();
		return _database;
	}

	initDB() async {
		Directory documentsDirectory = await getApplicationDocumentsDirectory();
		String path = join(documentsDirectory.path, "weatherDvt.db");
		return await openDatabase(path, version: 1, onOpen: (db) {},
				onCreate: (Database db, int version) async {
					await db.execute("CREATE TABLE locations ("
							"id INTEGER PRIMARY KEY AUTOINCREMENT,"
							"name TEXT,"
							"latitude REAL,"
							"longitude REAL"
							")");
				});
	}

	Future<Favourite> newFavourite(Favourite favourite) async {
		final db = await database;

		var raw = await db.rawInsert(
				"INSERT Into locations (name,latitude,longitude)"
						" VALUES (?,?,?)",
				[
					favourite.name,
					favourite.latitude,
					favourite.longitude,
				]);
		favourite.id = raw;
		return favourite;
	}

	Future<Favourite> getFavourite(int id) async {
		final db = await database;
		var res =
		await db.query("locations", where: "id = ?", whereArgs: [id]);
		return res.isNotEmpty ? Favourite.fromJson(res.first) : Null;
	}

	Future<List<Favourite>> getAllFavourites() async {
		final db = await database;
		var res = await db.query("locations");
		List<Favourite> list =
		res.isNotEmpty ? res.map((c) => Favourite.fromJson(c)).toList() : [];
		return list;
	}

	Future<Favourite> updateFavourite(Favourite favourite) async {
		final db = await database;
		var res = await db.update("locations", favourite.toJson(),
				where: "id = ?", whereArgs: [favourite.id]);

		favourite.id = res;
		print("DB UPDATE SUCCESSFUL $favourite");
		return favourite;
	}

	Future<bool> deleteFavourite(String id) async {
		final db = await database;
		db.delete("locations", where: "id = ?", whereArgs: [id]);

		return true;
	}

	Future<bool> deleteAllFavourites() async {
		final db = await database;
		db.rawDelete("Delete from locations");
		print("DB DELETED SUCCESSFUL");
		return true;
	}
}
